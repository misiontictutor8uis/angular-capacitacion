import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetallesUsuarioComponent } from './pages/detalles-usuario/detalles-usuario.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { RegistroComponent } from './pages/registro/registro.component';

const routes: Routes = [{ path: 'registro', component: RegistroComponent },
 { path: '', component: InicioComponent },
 { path: 'detalles-u/:id', component: DetallesUsuarioComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

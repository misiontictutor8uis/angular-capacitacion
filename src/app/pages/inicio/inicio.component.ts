import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  listaUsuarios : any = [];
  constructor(private service: UsuarioService) { }

  ngOnInit(): void {
    this.service.getAllUsuarios().subscribe( res => {
      console.log(res);
      this.listaUsuarios = res;
    } )
  
    }

  borrarUsuario(id:string){
    this.service.deleteUsuario(id).subscribe()
  }

}

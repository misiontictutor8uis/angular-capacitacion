import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-detalles-usuario',
  templateUrl: './detalles-usuario.component.html',
  styleUrls: ['./detalles-usuario.component.css']
})
export class DetallesUsuarioComponent implements OnInit {

  id = "";
  usuario: any;
  constructor(private service: UsuarioService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.id = this.route.snapshot.paramMap.get("id")!;

    this.service.getUsuario(this.id).subscribe( res => {
      console.log(res);
      this.usuario = res;
    } )

  }

  guardar(form: NgForm){
    this.service.updateUsuario(this.id, form.value).subscribe()
  }

}

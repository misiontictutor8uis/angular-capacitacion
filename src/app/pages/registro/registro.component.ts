import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent {

  constructor(private service: UsuarioService) { }

  guardar( formu : NgForm ) {
    console.log(formu.value);
    this.service.createUsuario( formu.value ).subscribe( res => console.log(res) )
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url = "http://localhost:3000/api/"

  constructor(private http: HttpClient) { }

  createUsuario(usuario: any) { 
    return this.http.post(this.url + "create-usuario", usuario);
  }

  getAllUsuarios(){
    return this.http.get(this.url + "get-usuarios");
  }

  getUsuario(id: string){
    return this.http.get(this.url + "get-usuario/" + id);
  }

  updateUsuario(id: string,usuario: object){
    return this.http.put(this.url + "update-usuario/" + id, usuario);
  }

  deleteUsuario(id: string){
    return this.http.delete(this.url + "delete-usuario/" + id);
  }

}
